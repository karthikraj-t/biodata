package com.example.biodata

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout

class listUser : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_user)

        val list:RecyclerView = findViewById(R.id.list)

        var alluser = OpenHelper(this).getAll()

        list.layoutManager = LinearLayoutManager(this,LinearLayout.VERTICAL,false)
        var adapter = list_items(alluser)
        list.adapter = adapter
    }
}

package com.example.biodata

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import java.util.*
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.MotionEvent
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.lang.Exception


class MainActivity : AppCompatActivity() {

    private val PICK_IMAGE:Int = 0
    private val GET_IMAGE:Int = 1
    lateinit var dob:EditText
    lateinit var photo:ImageView
    var byteArray:ByteArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        dob = this.findViewById(R.id.date)
        val name:EditText = findViewById(R.id.name)
        photo = findViewById(R.id.photo)
        val address:EditText = findViewById(R.id.address)
        val blood:EditText = findViewById(R.id.blood)
        val phone:EditText = findViewById(R.id.phone)
        val email:EditText = findViewById(R.id.email)
        val add:Button = findViewById(R.id.add)
//        val show:Button = findViewById(R.id.show)
        val openHelper = OpenHelper(this)
        val list: RecyclerView = findViewById(R.id.list)

        var alluser = OpenHelper(this).getAll()

        list.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        var adapter = list_items(alluser)
        list.adapter = adapter

        dob.setOnTouchListener { view, motionEvent ->

            if(motionEvent.action == MotionEvent.ACTION_UP)
            {
                val calendar = Calendar.getInstance()
                val year: Int = calendar.get(Calendar.YEAR)
                val month: Int = calendar.get(Calendar.MONTH)
                val day: Int = calendar.get(Calendar.DAY_OF_MONTH)

                val picker: DatePickerDialog = DatePickerDialog(this, DatePickerDialog.OnDateSetListener(function = { datePicker, year, month, day ->
                    dob?.setText("$day/$month/$year")
                }), year, month, day)
                picker.show()
                true
            }
            false
        }

        photo.setOnClickListener{
            showPictureDialog()
        }

        add.setOnClickListener {
            val n=if(TextUtils.isEmpty(name.text.trim()))
            {
                name.setError("required")
                false
            }
            else { true }
            val a=if(TextUtils.isEmpty(address.text.trim()))
            {
                address.setError("required")
                false
            }
            else { true }
            val d=if(TextUtils.isEmpty(dob.text.trim()))
            {
                dob.setError("required")
                false
            } else { true}
            val b=if(TextUtils.isEmpty(blood.text.trim()))
            {
                blood.setError("required")
                false
            }else {true}
            val e=if(TextUtils.isEmpty(email.text.trim()))
            {
                email.setError("required")
                false
            }else { true }
            val p=if(TextUtils.isEmpty(phone.text.trim()))
            {
                phone.setError("required")
                false
            }else { true }
            var pho:Boolean = false

            byteArray?.let {
                pho = true
            } ?: run{
                Toast.makeText(this,"Pleace upload Profile picture..",Toast.LENGTH_LONG).show()
                pho = false
            }

            if(n==true && a==true && d==true && b==true && p==true && e==true && pho==true)
            {
                val photo_id=openHelper.addPhoto(byteArray!!)
                val address_id = openHelper.addAddress(address.text.toString())
                var user = User(name.text.toString(),address_id,"",dob.text.toString(),blood.text.toString(),phone.text.toString(),email.text.toString(),"",photo_id,"".toByteArray())
                val r = openHelper.addUser(user)
                //Toast.makeText(this,"Saved",Toast.LENGTH_LONG).show()
                
                clearForm()
                var alluser = OpenHelper(this).getAll()
                var adapter = list_items(alluser)
                list.adapter = adapter
            }
        }
    }
    fun showPictureDialog()
    {
        var alertDialog = AlertDialog.Builder(this)
        var choose = arrayOf("Gallery","Camera")
        alertDialog.setItems(choose,{dialogInterface, i ->
            when(i)
            {
                0 -> chooseGallery()
                1 -> chooseCamera()
            }
        })
        alertDialog.show()
    }

    fun chooseCamera()
    {
        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,arrayOf(android.Manifest.permission.CAMERA),101  )
        }
        else
        {
            val intent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, GET_IMAGE)
        }
    }

    fun chooseGallery()
    {
        var intent: Intent = Intent()
        intent.setType(("image/*"))
        intent.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(Intent.createChooser(intent,"select picture"),PICK_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try{
            if(resultCode == Activity.RESULT_OK)
            {
                if(requestCode == PICK_IMAGE)
                {
                    //val p_bitmap=data!!.getData() as Bitmap
                    val uri: Uri = data!!.getData()
                    val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, uri)
                    photo.setImageBitmap(bitmap)

                    val stream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.PNG,70,stream)
                    byteArray = stream.toByteArray()
                }
                else if(requestCode == GET_IMAGE)
                {
                    val bitmap:Bitmap = data?.extras?.get("data") as Bitmap
                    photo.setImageBitmap(bitmap)

                    val stream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG,70,stream)
                    byteArray = stream.toByteArray()
                }
            }
        }
        catch (e:Exception)
        {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when(requestCode)
        {
            GET_IMAGE ->{
                if(grantResults.size>0)
                {
                    grantResults[0]== PackageManager.PERMISSION_GRANTED
                }
            }
        }
    }

    fun clearForm(){
        name.setText("")
        address.setText("")
        dob.setText("")
        blood.setText("")
        phone.setText("")
        email.setText("")
        photo.setImageResource(R.mipmap.round_person_outline_black_48)
        byteArray = null
    }
}
class User{

    val name:String
    val address:String
    val address_id:Int
    val dob:String
    val blood:String
    val phone:String
    val email:String
    val created_date:String
    val photo_id:Int
    val photo:ByteArray
    constructor(
        name:String,
        address_id:Int,
        address:String,
        dob:String,
        blood:String,
        phone:String,
        email:String,
        created_date:String,
        photo_id:Int,
        photo:ByteArray)
    {
        this.name=name
        this.address=address
        this.address_id=address_id
        this.dob=dob
        this.blood=blood
        this.phone=phone
        this.email=email
        this.created_date=created_date
        this.photo_id=photo_id
        this.photo=photo
    }
}

package com.example.biodata

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.lang.Exception

class OpenHelper(context:Context) : SQLiteOpenHelper(context,DATABASE_NAME,null,DATABASE_VERSION)
{
    override fun onCreate(p0: SQLiteDatabase?) {
        val PHOTO_TABLE ="CREATE TABLE $PHOTO_DETAILS ($PHOTO_ID INTEGER PRIMARY KEY AUTOINCREMENT, $PHOTO BLOB)"
        p0?.execSQL(PHOTO_TABLE)

        val ADDRESS_TABLE = "CREATE TABLE $ADDRESS_DETAILS ($ADDRESS_ID INTEGER PRIMARY KEY AUTOINCREMENT, $ADDRESS VARCHAR(50))"
        p0?.execSQL(ADDRESS_TABLE)

        val EMP_DETAILS = "CREATE TABLE $EMP_DETAILS ($EMP_ID INTEGER PRIMARY KEY AUTOINCREMENT, $NAME VARCHAR(30),$DOB VARCHAR(10), $BLOOD VARCHAR(10)" +
                ",$PHONE VARCHAR(13),$EMAIL VARCHAR(30),$CREATED_DATE DATE,$PHOTO_ID INTEGER, $ADDRESS_ID INTEGER, CONSTRAINT fk_address " +
                "FOREIGN KEY($ADDRESS_ID) REFERENCES $ADDRESS_DETAILS($ADDRESS_ID)," +
                "CONSTRAINT fk_photo " +
                "FOREIGN KEY($PHOTO_ID) REFERENCES $PHOTO_DETAILS($PHOTO_ID))"
        p0?.execSQL(EMP_DETAILS)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        val drop_emp ="DROP TABLE IF EXISTS $EMP_DETAILS"
        p0?.execSQL(drop_emp)
        val drop_photo = "DROP TABLE IF EXISTS $PHOTO_DETAILS"
        p0?.execSQL(drop_photo)
        val drop_address = "DROP TABLE IF EXISTS $ADDRESS_DETAILS"
        p0?.execSQL(drop_address)
        onCreate(p0)
    }

    fun addPhoto(byteArray: ByteArray):Int
    {
        val id:Int
        val db=this.writableDatabase
        val INSERT_PHOTO ="INSERT INTO $PHOTO_DETAILS ($PHOTO) values("+byteArray+")"
        val contentValue =ContentValues()
        contentValue.put(PHOTO,byteArray)
        db.insert(PHOTO_DETAILS,null,contentValue)
        val get_last_id ="SELECT MAX($PHOTO_ID) as $PHOTO_ID FROM $PHOTO_DETAILS"
        val rs = db.rawQuery(get_last_id,null)
        Log.d("count",rs.count.toString())
        db.close()
        if(rs?.count!! >0)
        {
            rs?.moveToFirst()
            id=rs.getInt(rs.getColumnIndex(PHOTO_ID))
            return id
        }
        return 0
    }

    fun addAddress(address:String):Int
    {
        var id:Int
        val db=this.writableDatabase
        val INSERT_ADDRESS = "INSERT INTO $ADDRESS_DETAILS ($ADDRESS) VALUES('"+address+"')"
        db.execSQL(INSERT_ADDRESS)
        val get_last_id = "SELECT MAX($ADDRESS_ID) AS $ADDRESS_ID FROM $ADDRESS_DETAILS"
        val rs = db.rawQuery(get_last_id,null)
        if(rs?.count!!>0)
        {
            rs?.moveToFirst()
            id=rs?.getInt(rs?.getColumnIndex(ADDRESS_ID))
            return id
        }
        db.close()
        return 0
    }
    fun addUser(user:User)
    {
        try {

            val db = this.writableDatabase
            val INSERT_EMP = "INSERT INTO $EMP_DETAILS ($NAME,$DOB,$BLOOD,$PHONE,$EMAIL,$CREATED_DATE,$PHOTO_ID,$ADDRESS_ID) VALUES('" +
                    user.name+"','"+user.dob+"','"+user.blood+"','"+user.phone+"','"+user.email+"',datetime(),"+user.photo_id+","+user.address_id+")"
            val r=db.execSQL(INSERT_EMP)
            val get_last_id = "SELECT MAX($EMP_ID) AS $EMP_ID FROM $EMP_DETAILS"
            val rs = db.rawQuery(get_last_id,null)
            Log.d("emp_inserted",rs.count.toString())
            if(rs?.count!!>0)
            {
                rs?.moveToFirst()
                val id=rs?.getInt(rs?.getColumnIndex(ADDRESS_ID))
                Log.d("id",id.toString())
            }
            db.close()
            return r
        }
        catch (e:Exception)
        {
            e.printStackTrace()
        }
    }
    fun getAll():MutableList<User>{
        val db = this.writableDatabase
        val SELECT = "SELECT E.$NAME AS $NAME, E.$DOB AS $DOB, E.$BLOOD AS $BLOOD, E.$PHONE AS $PHONE, " +
                "E.$EMAIL AS $EMAIL,E.$CREATED_DATE AS $CREATED_DATE, P.$PHOTO AS $PHOTO, A.$ADDRESS AS $ADDRESS FROM $EMP_DETAILS E " +
                "INNER JOIN $PHOTO_DETAILS P ON P.$PHOTO_ID = E.$PHOTO_ID INNER JOIN $ADDRESS_DETAILS A " +
                "ON A.$ADDRESS_ID = E.$ADDRESS_ID ORDER BY E.$CREATED_DATE DESC"
        Log.d("select query",SELECT)
        val r = db.rawQuery(SELECT,null)
        Log.d("count",r.count.toString())
        var allUser:MutableList<User> = mutableListOf()
//        if(r?.moveToFirst()!!)
//        {
            while (r.moveToNext())
            {
                var user = User(r.getString(r.getColumnIndex(NAME)),0,r.getString(r.getColumnIndex(ADDRESS)),r.getString(r.getColumnIndex(DOB)),r.getString(r.getColumnIndex(
                    BLOOD)),r.getString(r.getColumnIndex(PHONE)),r.getString(r.getColumnIndex(EMAIL)),r.getString(r.getColumnIndex(CREATED_DATE)),0,r.getBlob(r.getColumnIndex(PHOTO)))
                allUser.add(user)
            }
        //}
        Log.d("length",allUser.size.toString())
        allUser.forEach { Log.d("name : ",it.name) }
        db.close()
        return allUser
    }
    companion object {
        val DATABASE_NAME = "EMP.db"
        val DATABASE_VERSION = 1

        val PHOTO_DETAILS ="photo_details"
        val PHOTO_ID = "p_id"
        val PHOTO = "photo"

        val EMP_DETAILS = "emp_details"
        val EMP_ID = "emp_id"
        val NAME = "emp_name"
        val DOB = "dob"
        val BLOOD = "blood"
        val PHONE = "phone"
        val EMAIL = "email"
        val CREATED_DATE = "created_date"

        val ADDRESS_DETAILS = "address_details"
        val ADDRESS_ID = "address_id"
        val ADDRESS = "emp_address"
    }
}
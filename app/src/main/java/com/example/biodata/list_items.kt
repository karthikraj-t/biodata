package com.example.biodata

import android.graphics.BitmapFactory
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class list_items(var alluser:MutableList<User>) : RecyclerView.Adapter<list_items.ViewHolder>()
{
    class ViewHolder(view: View):RecyclerView.ViewHolder(view)
    {
        fun bindView(user:User)
        {
            val name:TextView = itemView.findViewById(R.id.name)
            val address:TextView = itemView.findViewById(R.id.address)
            val dob:TextView = itemView.findViewById(R.id.dob)
            val blood:TextView = itemView.findViewById(R.id.blood)
            val phone:TextView = itemView.findViewById(R.id.phone)
            val email:TextView = itemView.findViewById(R.id.email)
            val photo:ImageView? = itemView.findViewById(R.id.img)
            name.text = "Name : " +user.name
            address.text ="Address : " +user.address
            dob.text = "DOB : " +user.dob
            blood.text ="Blood Group : "+ user.blood
            phone.text ="Phone No : "+ user.phone
            email.text ="Email ID : "+ user.email

            var byteArray = user.photo
            var bitmap =BitmapFactory.decodeByteArray(byteArray, 0, byteArray?.size!!)
            photo?.setImageBitmap(bitmap)
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.activity_list_items,p0,false)
        return ViewHolder(v)
    }

    override fun getItemCount():Int{
        return alluser.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val ul:User = alluser[p1]
        p0.bindView(ul)
    }
}
